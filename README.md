# MAS

### Documentos relacionados con una compra denegada de Supermercados MAS

###### Realización de una compra que se empezó haciendo y se acabó siendo investigado e interrogado por la policía.

En estos archivos se puede apreciar el contenido de las distintas páginas de los textos legales de la web de Supermercados MAS.

En cada uno de los documentos se indica tanto la dirección URL como la fecha y hora que el propio navegador pone en su descarga.

Se puede destacar tanto los contenidos del día 13 de septiembre, idénticos al día anterior en el que se realizaron las compras.  Así como el que hayan puesto la web inactiva en mantenimiento el día 14, y además cómo el día 16 ya hicieron algunos cambios en sus textos legales y promociones, en el fichero de Condiciones generales de compra_2.

También hay algún ejemplo del proceso de un pedido, de cómo se indica la cantidad invitando a colocar algún código promocional, y cómo aparece la lista de pedidos una vez finalizada.

### Si quieres descargarte un ZIP con todos los documentos, puedes descargarte el fichero MAS_web_textos_legales.zip